<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Prendas;
use app\models\Categorias;
use app\models\Caracteristicas;
use app\models\Fotos;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        /**
         * consula para los productos de portada
         */
        $consulta = Prendas::find()->where([
            'portada' => 1
        ]); // select * from prendas where portada = 1;
        
        // ejecutar la consulta sin modelo
        //Yii::$app->db->createCommand('select * from prendas where portada = 1');
        //$dataProvider=new SqlDataProvider([
        //    "sql" => "select * from prendas where portada = 1"
        //]);
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        /**
         * Mostrar productos en portada en carousel
         */
        
        /**
         * crear un array para el carousel
         */
        
        $paraCarousel=[];
        
        $prendas=$consulta->all();
        foreach ($prendas as $prenda){
            $imagen=Html::img("@web/imgs/" . $prenda->id . "/" . $prenda->foto,['class'=>'mx-auto col-lg-12 pl-0 pr-0']); // foto de la prenda
            $precio='<div class="bg-white text-dark mb-2 p-2">' . $prenda->precio . '</div>'; // precio prenda
            $boton=Html::a("Ver mas",["site/ver","id"=>$prenda->id],["class"=>"btn btn-primary"]); // boton ver mas
            
            $paraCarousel[]=[
                'content' => $imagen,
                'caption' => $precio . $boton,   
            ];
        }
        
        //var_dump($prendas);
        //var_dump($paraCarousel);
        //exit;
        
        
       
        
        /**
         * Para motrar las ofertas en un carousel
         */
        // hacer una de los productos en oferta
        
        $consulta1=Prendas::find()->where([
            "oferta" => 1
        ]);
        
        
        // creo un dataprovider para los productos en oferta
        $dataProvider1= new ActiveDataProvider([
            "query" => $consulta1
        ]);
        // ejecuto la consulta
      
        $prendasOferta=$consulta1->all();
        
        // preparar el resultado de la consulta para un carousel de bootstrap
        $carouselOferta=[];
        foreach ($prendasOferta as $prenda){
            
            $imagen=Html::img("@web/imgs/" . $prenda->id . "/" . $prenda->foto,['class'=>'mx-auto col-lg-12 pl-0 pr-0']); // foto de la prenda
            
            $precio='<div class="bg-white text-dark mb-2 p-2">' . $prenda->precio . '</div>'; // precio prenda
            
            $descuento='<div class="bg-white text-dark mb-2 p-2">' . $prenda->descuento .'% </div>'; // precio prenda
            
            $boton=Html::a("Ver mas",["site/ver","id"=>$prenda->id],["class"=>"btn btn-primary"]); // boton ver mas
            
            $carouselOferta[]=[
                'content' => $imagen,
                'caption' => $precio . $descuento . $boton,   
            ];
        }
        
        
        
        return $this->render('index',[
            "dataProvider" => $dataProvider, // para el listview de productos portada
            "paraCarousel" => $paraCarousel, // para el carousel de los productos en portada
            "carouselOferta" => $carouselOferta, // para el carousel de los productos en oferta
            "dataProvider1" => $dataProvider1, // para el listview de productos en oferta
        ]);
    }
    
    
    
    /**
     * esta accion debe mostrarme todos los datos del producto
     * @param type $id
     */
    public function actionVer($id) {
       $consulta= Prendas::findOne($id);
       //$consulta= Prendas::find()->where(["id"=>id])->one();
       
       //$categoria=$consulta->getCategoria0()->one();
       $categoria=$consulta->categoria0;
           
       
       return $this->render("ver",[
           "model" => $consulta,
           "categoria" => $categoria,
       ]);
    }
    
    
    
    public function actionCazadoras() {
        /**
         * consula para los productos que sean cazadoras de hombre
         */
        
        // busco la categoria de cazadoras de hombres
        $categoria = Categorias::find()->where([
            'tipo' => 'hombre',
            'subtipo' => 'cazadora'
        ])->one(); // select * from categorias where tipo="hombre" AND subtipo="cazadora"; 
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 2;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Cazadoras",
            "tipo" => "Hombre",
            "subtipo" => "Cazadora"
        ]);
        
    }
    
    public function actionPantalonesh() {
        /**
         * consula para los productos que sean pantalones de hombre
         */
        
        // busco la categoria de pantalones de hombres
        $categoria = Categorias::find()->where([
            'tipo' => 'hombre',
            'subtipo' => 'pantalón'
        ])->one(); // select * from categorias where tipo="hombre" AND subtipo="pantalón"; 
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 1;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Pantalones",
            "tipo" => "Hombre",
            "subtipo" => "Pantalón"
        ]);
    }
    
    public function actionSudaderas() {
        /**
         * consula para los productos que sean sudaderas de hombre
         */
        
        // busco la categoria de sudaderas de hombres
        $categoria = Categorias::find()->where([
            'tipo' => 'hombre',
            'subtipo' => 'sudadera'
        ])->one(); // select * from categorias where tipo="hombre" AND subtipo="sudadera"; 
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 3;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Sudaderas",
            "tipo" => "Hombre",
            "subtipo" => "Sudadera"
        ]);
    }
    
    public function actionAbrigos() {
        /**
         * consula para los productos que sean abrigos de mujer
         */
        
        // busco la categoria de abrigos de mujer
        $categoria = Categorias::find()->where([
            'tipo' => 'mujer',
            'subtipo' => 'abrigo'
        ])->one(); // select * from categorias where tipo="mujer" AND subtipo="abrigo"; 
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 6;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Abrigos",
            "tipo" => "Mujer",
            "subtipo" => "Abrigo"
        ]);
    }
    
    public function actionPantalonesm() {
        /**
         * consula para los productos que sean pantalones de mujer
         */
        
        // busco la categoria de abrigos de mujer
        $categoria = Categorias::find()->where([
            'tipo' => 'mujer',
            'subtipo' => 'pantalón'
        ])->one(); // select * from categorias where tipo="mujer" AND subtipo="pantalón"; 
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 4;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Pantalones de mujer",
            "tipo" => "Mujer",
            "subtipo" => "Pantalón"
        ]);
    }
    
    public function actionFaldas() {
        /**
         * consula para los productos que sean faldas de mujer
         */
        
        // busco la categoria de faldas de mujer
        $categoria = Categorias::find()->where([
            'tipo' => 'mujer',
            'subtipo' => 'falda'
        ])->one(); // select * from categorias where tipo="mujer" AND subtipo="faldas"; 
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 5;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Faldas",
            "tipo" => "Mujer",
            "subtipo" => "Falda"
        ]);
    }
    
    public function actionNinos() {
        /**
         * consula para los productos que sean faldas de mujer
         */
        
        // busco la categoria de niños
        $categoria = Categorias::find()->where([
            'tipo' => 'nino',
        ])->one(); // select * from categorias where tipo="niño";
        
        $consulta = Prendas::find()->where([
            'categoria' => $categoria->id,
        ]);// select * from prendas where categoria = 7;
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Niños",
            "tipo" => "Niño",           
        ]);
    }
    
    public function actionCategoria($tipo) {
        
        // busco la categoria de niños
        $categorias = Categorias::find()->where([
            'tipo' => $tipo,
        ])->all(); // select * from categorias where tipo=??
        
        $idCategorias=[];
        foreach ($categorias as $categoria){
            $idCategorias[]=$categoria->id;
        }
        
        $consulta = Prendas::find()->where([
            'categoria' => $idCategorias,
        ]);// select * from prendas where categoria in (1,2,3);
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta
        ]);
        
        return $this->render('categoria',[
            "dataProvider" => $dataProvider,
            "titulo" => "Ropa de " . $tipo,
            "tipo" => $tipo,           
        ]);
    }
    
    public function actionOfertas() {
        
        /**
         * Consulta para los productos en oferta
         */
        $consula = Prendas::find()->where([
            "oferta" => 1,
        ]); // select * from prendas where oferta = 1
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consula
        ]);
        
        return $this->render('ofertas',[
            "dataProvider" => $dataProvider,
            "titulo" => "Ofertas",
            "migas" => true,
        ]);
                
    }
    
    
    /**
     * Accion que llamo desde las migas para que redireccione la accion
     * correspondiente al tipo y subtipo que estamos viendo
     * @param type $id
     * @return type
     */
    public function actionSubtipo($id) {
        
        $categoria= Categorias::findOne($id);
        
        if ($categoria->tipo=='hombre' && $categoria->subtipo=='cazadora') {
            return $this->redirect('cazadoras');
        }elseif ($categoria->tipo=='hombre' && $categoria->subtipo=='pantalón') {
            return $this->redirect('pantalonesh');
        }elseif($categoria->tipo=='hombre' && $categoria->subtipo=='sudadera'){
            return $this->redirect('sudaderas');
        }elseif($categoria->tipo=='mujer' && $categoria->subtipo=='abrigo'){
            return $this->redirect('abrigos');
        }elseif($categoria->tipo=='mujer' && $categoria->subtipo=='falda'){
            return $this->redirect('faldas');
        }elseif($categoria->tipo=='mujer' && $categoria->subtipo=='pantalón'){
            return $this->redirect('pantalonesm');
        }elseif($categoria->tipo=='nino' && $categoria->subtipo==' '){
            return $this->redirect('ninos');
        }    
    }
    
    
    public function actionDestacados() {
        $categorias= Categorias::find()->where([
            "tipo" => "hombre",    
        ])->all();
        
        // Opcion 1
        //$hombres= \yii\helpers\ArrayHelper::getColumn($categorias, "id");
        
        
        // Opcion 2
        $hombres=[];
        foreach ($categorias as $categoria){
            $hombres [] = $categoria->id;
        }
        
        $consulta= Prendas::find()->where([
            "portada" => 1,
            "categoria" => $hombres,
        ]);
        
        $dataProviderHom = new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        
        
        /**
         * Esto son las consultas y demas para mujeres
         */
        
        
        $categorias1 = Categorias::find()->where([
            "tipo" => "mujer",
        ])->all();
        
        $mujeres=[];
        foreach ($categorias1 as $categoriamu){
            $mujeres [] = $categoriamu->id;
        }
        
        $consultam = Prendas::find()->where([
            "portada" => 1,
            "categoria" => $mujeres,
        ]);
        
        $dataProvidermu = new ActiveDataProvider([
            "query" => $consultam
        ]);
                
        
        /**
         * Esto son las consultas y demas para ninños
         */
        $categorias2 = Categorias::find()->where([
            "tipo" => "nino",
        ])->all();
        
        $ninos=[];
        foreach ($categorias2 as $categoriani){
            $ninos[] = $categoriani->id;
        }
        
        $consultaNi = Prendas::find()->where([
            "portada" => 1,
            "categoria" => $ninos,
        ]);
         
        $dataProviderNino = new ActiveDataProvider([
            "query" => $consultaNi
        ]);
                
         return $this->render('destacado',[
            "dataProviderHom" => $dataProviderHom,
            "dataProvidermu" => $dataProvidermu,
            "dataProviderNino" => $dataProviderNino 
        ]);        
    }
}
