﻿DROP DATABASE IF EXISTS practica9;
CREATE DATABASE practica9;
USE practica9;

CREATE TABLE prendas(
    id int AUTO_INCREMENT,
    titulo varchar(100),
    referencia varchar(100),
    precio float,
    foto varchar (100) NOT NULL,
    portada boolean,
    oferta boolean,
    descuento float,
    categoria int NOT NULL,
    PRIMARY KEY(id)

  );

CREATE TABLE caracteristicas(
    id int AUTO_INCREMENT,
    prenda int NOT NULL,
    caracteristica varchar(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY(prenda, caracteristica)
  );

CREATE TABLE fotos(
    id int AUTO_INCREMENT,
    ruta varchar(100) NOT NULL,
    idprenda int,
    PRIMARY KEY(id)

  );

CREATE TABLE categorias(
    id int AUTO_INCREMENT,
    tipo varchar (100) NOT NULL,
    subtipo varchar(100),
    PRIMARY KEY (id),
    UNIQUE KEY (tipo,subtipo)
  );

ALTER TABLE prendas
  ADD CONSTRAINT fkPrendas_categorias FOREIGN KEY(categoria)
  REFERENCES categorias(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE caracteristicas
  ADD CONSTRAINT fkCaracteristicas_prendas FOREIGN KEY(prenda)
  REFERENCES prendas(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE fotos
  ADD CONSTRAINT fkFotos_prendas FOREIGN KEY (idprenda)
  REFERENCES prendas(id) ON DELETE CASCADE ON UPDATE CASCADE;