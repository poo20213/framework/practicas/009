<?php

use yii\widgets\ListView;

?>

<div>
    <h2>Hombre</h2>
</div>
<div>
<?php
echo ListView::widget([
    "dataProvider" => $dataProviderHom,
    "itemView" => "_destacado",
    "itemOptions" => [
        'class' => 'col-lg-4',
        ],
        "options" => [
            'class' => 'row',
        ],
        'layout'=>"{items}"
        ]);
?>
</div>
<h2>Mujer</h2>
<div>
<?php    
    echo $this->render('portadam',[
        "dataProvider" => $dataProvidermu,        
    ]);
?>
</div>
<h2>Niños</h2>
<div>
<?php    
    echo $this->render('portadani',[
        "dataProvider" => $dataProviderNino,
    ]);
?>
</div>