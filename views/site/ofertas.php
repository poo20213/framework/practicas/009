<?php
    use yii\widgets\ListView;
    
    if($migas){
        $this->params['breadcrumbs'][] = 'Ofertas';
    }
?>


<div>
<h1><?= $titulo?></h1>
</div>

<?php \dominus77\owlcarousel2\WrapCarousel::begin([
    //'theme' => \dominus77\owlcarousel2\Carousel::THEME_GREEN, // THEME_DEFAULT, THEME_GREEN
    //'tag' => 'div', // container tag name, default div
    //'containerOptions' => [/* ... */], // container html options
    'clientOptions' => [
        'loop' => true,
        'margin' => 10,
        'nav' => true,
        'autoplay' => true,
        'responsive' => [
            0 => [
                'items' => 1,
            ],
            600 => [
                'items' => 2,
            ],
            1000 => [
                'items' => 3,
            ],
        ],
    ],
]); ?>



    <?=    ListView::widget([
        "dataProvider" => $dataProvider,
        "itemView" => "_ofertas",
        "itemOptions" => [
        'class' => 'col-lg-12',
        ],
        "options" => [            
            'tag' => false,
        ],
        'layout'=>"{items}"
        ]);
    ?>

<?php \dominus77\owlcarousel2\WrapCarousel::end() ?>

