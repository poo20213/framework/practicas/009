<?php
use yii\helpers\Html;

$this->params['breadcrumbs'][] = [
    'label' => $categoria->tipo, 
    'url' => ['site/categoria','tipo'=>$categoria->tipo]
    ];

if(isset($categoria->subtipo)){
    $this->params['breadcrumbs'][] = [
        'label' => $categoria->subtipo, 
        'url' => ['site/subtipo','id'=>$categoria->id]
        ];
}

$this->params['breadcrumbs'][] = $model->titulo;
?>
<div>
    <?= $model->titulo ?>
</div>

<div>
    <?= $model->referencia ?>
</div>

<div>
    <?php
        foreach ($model->caracteristicas as $registro){
            echo "<li>$registro->caracteristica</li>";
        }
    ?>
</div>

<div>
    <?= $model->precio  ?>
</div>

<div>
    <?= $model->descuento ?>
</div>
    <?php
        echo newerton\fancybox\FancyBox::widget([
        'target' => 'a[rel=fancybox]',
        'helpers' => true,
        'mouse' => true,
        'config' => [
            'maxWidth' => '90%',
            'maxHeight' => '90%',
            'playSpeed' => 7000,
            'padding' => 0,
            'fitToView' => false,
            'width' => '70%',
            'height' => '70%',
            'autoSize' => false,
            'closeClick' => false,
            'openEffect' => 'elastic',
            'closeEffect' => 'elastic',
            'prevEffect' => 'elastic',
            'nextEffect' => 'elastic',
            'closeBtn' => false,
            'openOpacity' => true,
            'helpers' => [
                'title' => ['type' => 'float'],
                'buttons' => [],
                'thumbs' => ['width' => 68, 'height' => 50],
                'overlay' => [
                    'css' => [
                        'background' => 'rgba(0, 0, 0, 0.8)'
                    ]
                ]
            ],
        ]
    ]);

    ?>


<div>
    <?php
        echo Html::a(Html::img("@web/imgs/" . $model->id . "/" . $model->foto,[
               'class' => 'col-lg-4',
               
           ]), "@web/imgs/" . $model->id . "/" . $model->foto,// esta es la foto en miniatura
           [
               'class' => '',
               'rel' => 'fancybox'
           ]);
        foreach ($model->fotos as $registro){
           echo Html::a(Html::img("@web/imgs/" . $model->id . "/" . $registro->ruta,[
               'class' => 'col-lg-4',
               
           ]),"@web/imgs/" . $model->id . "/" . $registro->ruta, // esta es la foto que se ve en grande
           [
               'class' => '',
               'rel' => 'fancybox'
           ]); 
        }
    
    ?>
</div>
